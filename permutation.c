#include <stdlib.h>
#include <stdio.h>

void	print_mass(int *array, int len)
{
	int		i;

	i = 0;
	while (i < len)
	{
		printf("%d ", array[i]);
		i++;
	}
	printf("\n");
}

void	swap(int *x, int *y)
{
	int tmp;

	tmp = *x;
	*x = *y;
	*y = tmp;
}
void permute_rec(int *array,int i,int len)
{
	int j;

	if (len == i)
		print_mass(array,len);
	j = i;
	while(j < len)
	{
		swap(array+i,array+j);
		permute_rec(array,i+1,len);
		swap(array+i,array+j);
		j++;
	}
}

int permutation(int n)
{
	int *array;
	int l;
	int i;

	l = 1;
	i = 0;
	if (!(array = (int *)malloc(sizeof(int) * n)))
		return (0);
	while (l <= n)
	{
		array[i] = l;
		l++;
		i++;
	}
	permute_rec(array, 0, n);
	return (1);
}

int main(int ac, char **av)
{
	ac = ac + 0;
	permutation(atoi(av[1]));
	return (0);
}
